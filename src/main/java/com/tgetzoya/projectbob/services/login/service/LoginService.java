/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * LoginService.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.login.service;


import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import org.springframework.http.ResponseEntity;

/**
 * Interface for logging the user in and out.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @version 1.0
 */
public interface LoginService {
    /**
     * Returns a status of OK if the service is up and running.
     *
     * @return A status of OK if the service is running
     */
    ResponseEntity healthCheck();

    /**
     * Log the user in. On success, return an authentication token.
     *
     * @param loginModel the login data
     * @return the response on whether the login was successful
     */
    ResponseModel loginUser(LoginModel loginModel);
}
