/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * LoginServiceImpl.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.login.service;

import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import com.tgetzoya.projectbob.models.UserModel;
import com.tgetzoya.projectbob.models.error.ErrorModel;
import com.tgetzoya.projectbob.utils.auth.AuthUtils;
import com.tgetzoya.projectbob.utils.password.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Implementation for logging the user in and out.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @version 1.0
 */
@Component
@PropertySource("classpath:locations.properties")
public class LoginServiceImpl implements LoginService {
    /**
     * To get the urls from locations.properties file.
     */
    @Autowired
    Environment mEnv;

    /**
     * To be able to create a salt.
     */
    private SecureRandom mSecureRandom;

    /**
     * Make sure the randome number generator is initialized.
     */
    @PostConstruct
    public void init() {
        mSecureRandom = new SecureRandom();
    }

    /**
     * Returns a status of OK if the service is running.
     *
     * @return a status of OK if the service is running
     */
    @Override
    public ResponseEntity healthCheck() {
        return ResponseEntity.ok("{\"status\":\"ok\"}");
    }

    /**
     * Log the user in. On success, return an authentication token.
     *
     * @param loginModel the login data
     * @return the response on whether the login was successful
     */
    @Override
    public ResponseModel loginUser(LoginModel loginModel) {
        RestTemplate template = new RestTemplate();

        ResponseEntity<ResponseModel> validationResponse = template.postForEntity(
                mEnv.getProperty("url.validation.login"),
                loginModel,
                ResponseModel.class);

        if (validationResponse.getBody().getResult() != 0) {
            return validationResponse.getBody();
        }

        HttpEntity<LoginModel> loginEntity = new HttpEntity<>(loginModel, new HttpHeaders());

        ResponseEntity<ResponseModel<UserModel>> getUserResponse =
                template.exchange(
                        mEnv.getProperty("url.readwrite.readuser"),
                        HttpMethod.POST,
                        loginEntity,
                        new ParameterizedTypeReference<ResponseModel<UserModel>>() {
                        });

        if (getUserResponse.getBody().getResult() != 0) {
            return getUserResponse.getBody();
        }


        UserModel storedUser = getUserResponse.getBody().getResponseData();
        ByteBuffer attempedPassword = null;

        try {
            attempedPassword = PasswordUtils.generatePasswordHash(loginModel.getPassword(), storedUser.getSalt());
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            ErrorModel em = new ErrorModel("Could not test password.");
            ResponseModel rm = new ResponseModel();
            rm.setErrors(Arrays.asList(em));
            rm.setResult(1);

            return rm;
        }

        ResponseModel responseModel = new ResponseModel();

        try {
            loginModel.setAuthToken(AuthUtils.generateAuthToken(loginModel.getEmail()));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            ErrorModel em = new ErrorModel("Could not generate Auth Token.");
            ResponseModel rm = new ResponseModel();
            rm.setErrors(Arrays.asList(em));
            rm.setResult(1);

            return rm;
        }

        if (attempedPassword.equals(storedUser.getPassword())) {
            responseModel.setResult(0);
            responseModel.setResponseData(loginModel.getAuthToken());
        } else {
            ErrorModel em = new ErrorModel("Incorrect password.");
            responseModel.setErrors(Arrays.asList(em));
            responseModel.setResult(1);
        }

        loginModel.setId(storedUser.getId());

        ResponseEntity<ResponseModel> response = template.postForEntity(
                mEnv.getProperty("url.readwrite.writedata"),
                loginModel,
                ResponseModel.class);

        if (response.getBody().getResult() != 0) {
            return response.getBody();
        } else {
            return responseModel;
        }
    }
}
