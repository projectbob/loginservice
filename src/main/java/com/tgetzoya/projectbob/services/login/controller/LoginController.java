/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * LoginController.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.login.controller;

import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.services.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This controller handles logging the user in and out of the system.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@RestController
public class LoginController {
    /**
     * The actual reading and writing service
     */
    @Autowired
    private LoginService loginService;

    /**
     * Returns the string "OK" if the service is working.
     *
     * @return String "OK" if it's working
     */
    @RequestMapping(value = "/healthCheck",
            method = RequestMethod.GET,
            produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public ResponseEntity healthCheck() {
        return loginService.healthCheck();
    }

    /**
     * Log in the user.  Return the auth token on success, error code otherwise.
     *
     * @return ResponseModel auth token on success, error code otherwise.
     */
    @RequestMapping(value = "/loginUser",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity getUser(@RequestBody LoginModel loginModel) {
        return ResponseEntity.ok(loginService.loginUser(loginModel));
    }
}
